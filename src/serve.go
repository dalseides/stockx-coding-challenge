package main

import (
  "io"
  "fmt"
  "strings"
  "net/http"
  "database/sql"
  _ "github.com/lib/pq"
)

// https://www.calhoun.io/connecting-to-a-postgresql-database-with-gos-database-sql-package/
const (
  host     = "postgres"
  port     = 5432
  user     = "postgres"
  password = "mysecretpassword"
  dbname   = "postgres"
)

// https://thenewstack.io/building-a-web-server-in-go/
func hello (w http.ResponseWriter, r *http.Request) {
  fmt.Println("Hello")
  io.WriteString(w, "Hello")
}

func vote (w http.ResponseWriter, r *http.Request) {
  psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
    "password=%s dbname=%s sslmode=disable",
    host, port, user, password, dbname)

  db, err := sql.Open("postgres", psqlInfo)
  if err != nil {
    panic(err)
  }
  defer db.Close()

  sqlStatement := `
    INSERT INTO truesize (shoe, vote)
    VALUES ($1, $2)`
  _, err = db.Exec(sqlStatement, r.FormValue("shoe"), r.FormValue("vote"))
  if err != nil {
    panic(err)
  }

  fmt.Println("shoe is " + r.FormValue("shoe"))
  fmt.Println("vote is " + r.FormValue("vote"))
  http.Redirect(w, r, "/", http.StatusSeeOther)
}

func stats (w http.ResponseWriter, r *http.Request) {
  psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
    "password=%s dbname=%s sslmode=disable",
    host, port, user, password, dbname)

  db, err := sql.Open("postgres", psqlInfo)
  if err != nil {
    panic(err)
  }
  defer db.Close()

  sqlStatement := `SELECT shoe, AVG(vote) FROM truesize GROUP BY shoe;`
  var averages string

  rows, err := db.Query(sqlStatement)
  if err != nil {
    panic(err)
  }
  defer rows.Close()
  for rows.Next() {
    var shoe string
    var truesize float64
    err = rows.Scan(&shoe, &truesize)
    if err != nil {
      panic(err)
    }
    fmt.Println(shoe, truesize)
    averages += fmt.Sprintf("%s truesize: %f\n", shoe, truesize)
  }
  err = rows.Err()
  if err != nil {
    panic(err)
  }

  fmt.Println("Method is ", r.Method)
  io.WriteString(w, averages)
}

func initDB () {
  psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
    "password=%s dbname=%s sslmode=disable",
    host, port, user, password, dbname)

  db, err := sql.Open("postgres", psqlInfo)
  if err != nil {
    panic(err)
  }
  defer db.Close()

  sqlStatement := `CREATE TABLE truesize (id SERIAL PRIMARY KEY, shoe VARCHAR(255) NOT NULL, vote INTEGER NOT NULL);`

  row := db.QueryRow(sqlStatement)
  switch err := row.Scan(); err {
    case sql.ErrNoRows:
        fmt.Println("Table created")
    default:
        if !(strings.Contains(err.Error(), "already exists")) {
          panic(err)
        }
  }
}

func main(){
  initDB()
  http.HandleFunc("/vote", vote)
  http.HandleFunc("/stats", stats)
  http.HandleFunc("/hello", hello)
  // https://stackoverflow.com/questions/26559557/how-do-you-serve-a-static-html-file-using-a-go-web-server
  http.Handle("/", http.FileServer(http.Dir("./www")))

  http.ListenAndServe(":8000", nil)
}


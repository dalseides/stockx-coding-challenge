FROM nimmis/alpine-golang

# update wget
RUN apk update && apk add ca-certificates && update-ca-certificates && apk add openssl && apk add git
WORKDIR /app

# get the code
ADD https://davidaaronfischer.com/stockx_coding_challenge/src/serve.go .
ADD https://davidaaronfischer.com/stockx_coding_challenge/www/index.html www/

RUN go get

# run the code
CMD ["go", "run", "serve.go"]
